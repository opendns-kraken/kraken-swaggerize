'use strict';

var util = require('util'),
    _ = require('lodash'),
    Inflector = require('inflected'),
    manifest = require('./base.json');

class Swaggerize {
    constructor(name, version, description) {
        this.manifest = _.cloneDeep(manifest);
        this.manifest.info = _.merge(this.manifest.info, {
            title: name,
            version: version,
            description: description
        });

        this.constructor.manifestKeys.forEach((key) => {
          var setter = util.format('set%s', Inflector.camelize(key));

          this[setter] = (val) => {
            this[key] = val;
          };
        });
    }

    execute(models) {
        this.constructor.manifestKeys.forEach((key) => {
          var val = _.get(this, key, false);

          if (val) {
            this.manifest[key] = val;
          }
        });

        models.forEach((model) => {
            this.generateDefinitions(model);
            this.generatePaths(model);
        });

        return this.manifest;
    }

    generateDefinitions(model) {
        var responseSchema = model.responseSchema();
        var createUpdateSchema = model.createUpdateSchema();

        this.manifest.definitions[util.format('%sResponse', model.constructor.name)] = {
            type: 'object',
            required: this.inferRequired(responseSchema),
            properties: this.inferDefinitionProperties(responseSchema)
        };

        this.manifest.definitions[util.format('%sCreateUpdate', model.constructor.name)] = {
            type: 'object',
            required: this.inferRequired(createUpdateSchema),
            properties: this.inferDefinitionProperties(createUpdateSchema)
        };
    }

    generatePaths(model) {
        var resources = model.resources();

        resources.forEach((r) => {
            this.generatePath(model, r);
        });
    }

    generatePath(model, path) {
        this.generateCollectionPaths(model, path);
        this.generateResourcePaths(model, path);
    }

    generateCollectionPaths(model, path) {
        var pluralizedModel = Inflector.pluralize(model.constructor.name).toLowerCase();

        var collectionPath = util.format(
            '/%s',
            path.replace(/^\/|\/$/g, '').split('/').slice(0, -1).join('/')
        );

        this.manifest.paths[this.inferUntypedPath(collectionPath)] = {
            'x-swagger-router-controller': pluralizedModel,
            'x-kraken-collection-name': model.constructor.name
        };

        this.generateListPath(model, collectionPath);
        this.generateCreatePath(model, collectionPath);
    }

    generateResourcePaths(model, path) {
        var pluralizedModel = Inflector.pluralize(model.constructor.name).toLowerCase();

        this.manifest.paths[this.inferUntypedPath(path)] = {
            'x-swagger-router-controller': pluralizedModel,
            'x-kraken-collection-name': model.constructor.name
        };

        this.generateReadPath(model, path);
        this.generateDeletePath(model, path);
        this.generateUpdatePath(model, path);
    }

    generateListPath(model, path) {
        var pluralizedName = Inflector.pluralize(model.constructor.name);

        var description = util.format(
            'Return a collection of %s',
            Inflector.humanize(pluralizedName)
        );

        this.manifest.paths[this.inferUntypedPath(path)].get = {
            operationId: 'list',
            summary: pluralizedName,
            description: description,
            tags: [
                model.constructor.name
            ],
            parameters: this.inferPathProperties(model, path, false),
            responses: {
                default: {
                    description: description,
                    schema: {
                        type: 'array',
                        items: {
                            '$ref': util.format(
                                '#/definitions/%sResponse',
                                model.constructor.name
                            )
                        }
                    }
                }
            }
        };
    }

    generateReadPath(model, path) {
        var description = util.format(
            'Return a %s',
            Inflector.humanize(model.constructor.name)
        );

        this.manifest.paths[this.inferUntypedPath(path)].get = {
            operationId: 'read',
            summary: model.constructor.name,
            description: description,
            tags: [
                model.constructor.name
            ],
            parameters: this.inferPathProperties(model, path, false),
            responses: {
                default: {
                    description: description,
                    schema: {
                        '$ref': util.format(
                            '#/definitions/%sResponse',
                            model.constructor.name
                        )
                    }
                }
            }
        };
    }

    generateDeletePath(model, path) {
        var description = util.format(
            'Delete a %s',
            Inflector.humanize(model.constructor.name)
        );

        this.manifest.paths[this.inferUntypedPath(path)].delete = {
            operationId: 'delete',
            summary: model.constructor.name,
            description: description,
            tags: [
                model.constructor.name
            ],
            parameters: this.inferPathProperties(model, path, false),
            responses: {
                204: {
                    description: util.format(
                        '%s deleted',
                        Inflector.humanize(model.constructor.name)
                    )
                }
            }
        };
    }

    generateCreatePath(model, path) {
        var pluralizedName = Inflector.pluralize(model.constructor.name);

        var description = util.format(
            'Create a new %s',
            Inflector.humanize(model.constructor.name)
        );

        this.manifest.paths[this.inferUntypedPath(path)].post = {
            operationId: 'create',
            summary: pluralizedName,
            description: description,
            tags: [
                model.constructor.name
            ],
            parameters: this.inferPathProperties(model, path, true),
            responses: {
                default: {
                    description: description,
                    schema: {
                        '$ref': util.format(
                            '#/definitions/%sResponse',
                            model.constructor.name
                        )
                    }
                }
            }
        };
    }

    generateUpdatePath(model, path) {
        var pluralizedName = Inflector.pluralize(model.constructor.name);

        var description = util.format(
            'Update an existing %s',
            Inflector.humanize(model.constructor.name)
        );

        this.manifest.paths[this.inferUntypedPath(path)].put = {
            operationId: 'update',
            summary: pluralizedName,
            description: description,
            tags: [
                model.constructor.name
            ],
            parameters: this.inferPathProperties(model, path, true),
            responses: {
                default: {
                    description: description,
                    schema: {
                        '$ref': util.format(
                            '#/definitions/%sResponse',
                            model.constructor.name
                        )
                    }
                }
            }
        };
    }

    inferUntypedPath(path) {
        return path.split('/').map((piece) => {
            if (piece.indexOf(':') !== 1) {
                piece = piece.replace(/\:.*\}/, '}');
            }

            return piece;
        }).join('/');
    }

    inferPathProperties(model, path, isCreateOrUpdate) {
        isCreateOrUpdate = _.isUndefined(isCreateOrUpdate) ? false : isCreateOrUpdate;

        var params = _.fromPairs((path.match(/\{([^\/]+)\}/g) || []).map((match) => {
            match = match.substring(1, match.length - 1);

            return match.split(':');
        }));

        var properties = _.map(params, (type, key) => {
            return {
                name: key,
                in: 'path',
                type: type,
                required: true
            };
        });

        if (isCreateOrUpdate) {
            var modelName = model.constructor.name;

            properties.push({
                name: (modelName[0].toLowerCase() + modelName.substring(1)),
                in: 'body',
                description: util.format(
                    'The new %s you want to create',
                    Inflector.humanize(model.constructor.name)
                ),
                schema: {
                    '$ref': util.format(
                        '#/definitions/%sCreateUpdate',
                        model.constructor.name
                    )
                }
            });
        }

        return properties;
    }

    inferRequired(schema) {
        return _.chain(_.cloneDeep(schema))
            .map((params, key) => {
                params.key = key;

                return params;
            })
            .filter((params) => {
                return !!params.required;
            })
            .map((params) => {
                return params.key;
            })
            .value();
    }

    inferDefinitionProperties(schema) {
        var cloned = _.cloneDeep(schema);

        _.forEach(cloned, (properties, key) => {
            delete properties.required;
        });

        return cloned;
    }
}

Swaggerize.manifestKeys = [
  'version',
  'description',
  'host',
  'basePath',
  'schemes',
  'consumes',
  'produces'
];

module.exports = Swaggerize;